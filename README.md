# 信息系统分析与设计

![教材](./images/book.jpg)

## 实验背景-生产管理系统

- 系统背景
生产管理系统背景：某企业的业务是生产并销售一类机电产品。基本流程是接收客户订单，从供应商处采购原材料，生产加工为成品，然后将成品发货给客户。

- 业务流程
企业业务活动中有不少流程，比如订客户订单流程，采购流程，生产流程，出入库流程，发货流程，售后维修流程等。

- 数据
企业业务活动中有不少数据，如：员工，客户，供应商，物料，订单，物流单等。特别注意，物料数据采用BOM方式以树状结构存储。

- 数据分析与统计
企业需要一些数据分析与统计工作，如：MRP，排程排产，最低库存采购，成本及利润分析，导出各类统计报表等。

## 实验准备

- 每个同学必须有一个gitlab账号，并上传SSH公钥。
- 每4到5人一个组，每个组一个组长，由组长创建一个私有项目，项目名称:12班必须是is_experiment，34班必须是is_experiment_34，两个班名称不同以示区别。
- 由组长将组员加入到项目中，成员的角色是maintainer
- 由组长将老师也加入到项目中。老师的账号是zwdcdu，将老师的角色也设置为maintainer。
- 在项目中创建5个文件夹：test1,test2,test3,test4,test5。表示5次实验。每次实验放在各自的文件夹中。
- 每个目录中必须有一个文件README.md。注意大写小写。
- 在is_experiment根目录中README.md文件的内容是班级，用表格的形式写上项目成名的学号，姓名，git账号，介绍等信息。
- 在5个test目录中的README.md文件是主要的实验报告，说明每个实验的主要内容以及完成人员。除了README.md文件外，还必须有其他辅助文件。README.md文件通过链接，引用其他文件。
- 实验环境：文档编写工具用vscode或者其他支持git的工具。
- 实验环境：markdown,以及UML语言工具plantuml,mermaid等工具和标准书写文字和绘制图形，尽量不要使用word,excel,photoshop等工具。
- 每次实验中每个项目成员都必须有单独的提交痕迹，不能只是由一个同学提交，以区分团队成员的分工和工作量。原则上团队成员只维护自己的文件。
- 由于老师也是你项目中的成员，所以老师能看见你的项目并批阅，评估每个同学的工作量。
- 本项目是私有项目，因此同班的其他团队的人员是看不见的。
- 如果vscode中不能预览图像，要安装graphviz，下载[graphviz-2.38.msi](./graphviz-2.38.msi)。
- 在windows系统中安装git环境工具。下载[gitgfb_ttrar.rar](./gitgfb_ttrar.rar)

## 重要

- 每次提交时，必须写上姓名和提交原因，如“张三做了某工作”。让老师知道是你做的。计算你的工作量。也就是说每个test目录中的文件数量应该多于成员数量。但统管文件只有一个，即README.md。

## 首次实验-克隆项目

```sh
git clone 项目地址
```

## 再次实验

- git pull拉取其他成员的修改，可以通过vscode的界面，也可以通过git pull命令
  - 命令行方法
    - 进入项目目录，打开git bash
    - 运行git pull

- 通过vscode编辑，编写自己的代码/文档，然后转到git提交自己的修改部分。
- git提交可以通过vscode的界面，也可以通过命令行，方法是进入项目目录，打开git bash，运行如下命令

    ```sh
    git add .
    git commit -m '张三，做了什么修改...'
    git push
    git pull
    ```

## 实验1：生产管理系统流程分析

- 实验目的
  - 分析生产管理系统中的部分流程，以文字/数据流图/业务流程图的绘制出一些简单的业务流程。参见[流程图样例](./flow_sample.md)。
  - 学习通过git团队开发的原理和方法。
- 最后提交日期:2022-3-18

## 实验2：生产管理系统用例建模

- 实验目的
  - 分析生产管理系统中的部分用例，以用例图/文字和表格方式表示。
- 最后提交日期:2022-3-25

## 实验3：生产管理系统领域对象建模

- 实验目的
  - 分析生产管理系统中的部分类图(class diagram)以及一些类的对象图(object diagram)，以用例图/文字和表格方式表示。
  - 类图要尽量体现类与类之间的关系：依赖关系(Dependency)，关联关系(Association)，聚合关系(Aggregation)，组合关系(Composition)，继承关系(Inheritance)
  - 分析产品的BOM(Bill of Material)结构。
- 最后提交日期:2022-4-1

## 实验4：生产管理系统实体设计

- 实验目的
  - 根据生产管理系统中的对象，创建部分表(Table)，构建实体联系图（Entity Relationship Diagram）。
  - 表(table)样例：
  supplier_part_purchase_setup供应商物料采购配置表

    |描述|主键外键|类型|可以为空|说明|
    |:---|:---|:---|:---|:---|
    |id|PK|int|N|自动增加|
    |part_no|FK|nvarchar(100)|N|物料编号,part表外键|
    |min_quantity||int|N|最小采购数量,default=1，最小采购数量|
    |multiple||money|N|供应倍数,default=1，必须>0|
    |min_package||money|N|最小包装数,default=1，必须>0,仅供参考，不参加运算|
    |box_quantity||money|Y|整箱数量,default=1，必须>0,仅供参考，不参加运算|
    |purchase_cycle||int|N|采购周期，单位是天|
    |supplier_dept_id|FK|int|Y|供应商ID,[supplier供应商表](./表设计_基础.md#supplier供应商表)外键，如果为空，表示适合于所有供应商|

  - [ER图mermaid格式](https://mermaid-js.github.io/mermaid/#/entityRelationshipDiagram)
- 最后提交日期:2022-4-9

## 实验5 生产管理系统UI及接口设计 | [返回](./README.md)

- 实验目的
  - 本实验的目的是设计生产管理系统的部分UI界面。界面设计应当清晰，直观，符合人体感观。
  - 推荐使用软件Axure设计UI界面，实现一些需求的页面，并可在页面之间跳转。并通过GitLab Pages发布设计结果。并提供链接地址，如果gitlab的pages地址发布失败，可以用于其他外网服务器发布，只要能给出外网的url地址就行。
  - 如果你的项目UI界面发布在https://app.axure.cloud/上，可以邀请我加入，我的账号是zwdcdu@cdu.edu.cn.
  - 参见axure源文件bom.rp以及public目录（axure发布的文件）
  - UI界面样例的最终的超链接地址是： https://zwdcdu.gitlab.io/is_analysis/test5/
- 最后提交日期:2022-4-16

- 实验5要点：
  - 将文件.gitlab-ci.yml复制到你的项目的根目录，修改最后一行的名称为你的项目的主分支名称，main或者master。
  - 将axure生成的html文件及子目录复制到项目的public子目录
  - 在GitLab页面上执行:CI/CD->流程线->运行流水线
  - web url地址获取:GitLab页面上执行:设置->Pages,在访问页面查看url地址。

## 期末考核(test6)

- 期末考核题目：基于BOM的生产管理系统的分析与设计
- 期末考核要求
  - 以小组为单位编写一套“基于BOM的生产管理系统”的分析与设计。
  - 某企业的业务是生产并销售一类机电产品。基本流程是接收客户订单，从供应商处采购原材料，生产加工为成品，然后将成品发货给客户。企业日常工作划分为采购，工艺，销售，售后服务，生产配料，外协加工，库存管理，财务管理等模块。
  - 请选择2至3个模块，以BOM清单为依据进行系统分析与设计
  - 设计格式参见教材附录B：项目实践的建议与案例。
- 文档提交要求
  - 按小组为单位提交，小组成员分工明确，工作量平均。
  - 文档提交到test6子目录,与test1-test5是平级目录
  - UI设计发布为gitlab pages，同test5一样，还是放在public目录，并说明URL地址,如果gitlab的pages地址发布失败，可以用于其他外网服务器发布，只要能给出外网的url地址就行
  - 如果你的项目UI界面发布在https://app.axure.cloud/上，可以邀请我加入，我的账号是zwdcdu@cdu.edu.cn
  - 项目需要按小组进行网上课程答辩
- 项目答辩日期：
  - 1/2班在第10周星期四（2022-4-21日）下午上课时间
  - 3/4班在第10周星期五（2022-4-22日）下午上课时间
- 文档最后提交日期:2022-4-30日

- 评分项及评分标准（总分100分）：

    |评分项|评分标准|分数|
    |:-------|:----------------------|:------|
    |答辩情况|答辩过程论述清楚，有理有据|20|
    |用例图及规约|用例图完整，准确，能够完全体现需求|10|
    |顺序图与活动图|顺序图能够完全描述用例的设计思路和业务流程，活动图能够描述部分算法的流程。|10|
    |类图|类图能够完整，准确反映业务的需数据的组织结构|10|
    |数据库设计|数据库表的设计来自于类图，合理|10|
    |UI界面及接口设计|界面设计美观，清晰，合理，能够完全反映用例图的设计思路|20|
    |文档整体性|文档内容详实、规范，美观大方，用例图，类图、数据库、界面相互印证，相互依赖，环环相扣|20|

## 参考资料

- 绘制方法参考：[PlantUML标准](http://plantuml.com)
- [PlantUML GitHub 官方网站](https://github.com/plantuml/plantuml)
- PlantUML服务: http://plantuml.com/zh/server
- plantuml在线编辑器： http://www.plantuml.com/plantuml
- Markdown格式参考： https://www.jianshu.com/p/b03a8d7b1719
- Git简书 https://git-scm.com/book/zh/v2
- Git分支 https://git-scm.com/book/zh/v1/Git-%E5%88%86%E6%94%AF
- Github 简明教程-操作标签 https://www.cnblogs.com/tracylxy/p/6439089.html
- Git菜鸟教程 http://www.runoob.com/git/git-tutorial.html
- [GitWindows客户端](./gitgfb_ttrar.rar)
- 版本控制样例参见：https://github.com/oracle/db-sample-schemas
- 文档编写工具 Sphinx 使用手册 https://of.gugud.com/t/topic/185 https://zh-sphinx-doc.readthedocs.io/en/latest/contents.html
- [GitHub开发者接口V3](https://maintainer.github.com/v3/)
- [mermaid](https://gitee.com/mirrors/mermaid/blob/develop/README.zh-CN.md)
- [gitlab](https://gitlab.com/users/sign_in?redirect_to_referer=yes)
- [GitLab 之 PlantUML 的配置及使用](https://blog.csdn.net/aixiaoyang168/article/details/76888254)
- [gitlab markdown](https://docs.gitlab.com/ee/user/markdown.html)
- 创建github账号，用github账号登录gitlab
- [git 多账号 ssh-key 管理（github和gitlab共同使用）](https://blog.csdn.net/qq_30227429/article/details/80229167)

## 网课地址

- 手机端使用超星学习通,电脑端使用腾讯会议
- 请大家在上课前10分钟查询本页面！老师发布具体上课通知。
- 通过学习通App签到
- 腾讯会议网课地址
  - 12班4月14日星期四网课：腾讯会议号:已完成。
  - 12班第10周星期四网课：腾讯会议号:901-212-282，无密码

    ```text
    会议主题：12班星期四系统分析课程
    会议时间：2022/04/21 14:00-17:30 (GMT+08:00) 

    点击链接入会，或添加至会议列表：
    https://meeting.tencent.com/dm/IfHPBqMvb7XO

    #腾讯会议：901-212-282

    手机一键拨号入会
    +8675536550000,,901212282# (中国大陆)
    +85230018898,,,2,901212282# (中国香港)

    根据您的位置拨号
    +8675536550000 (中国大陆)
    +85230018898 (中国香港)
    ```

  - 34班4月15日星期五网课：腾讯会议号:已完成，无密码
  - 34班第10周星期五网课：腾讯会议号:173-782-419，无密码

    ```text
    成都大学赵卫东 邀请您参加腾讯会议
    会议主题：34班第10周系统分析网课
    会议时间：2022/04/22 14:00-17:30 (GMT+08:00) 

    点击链接入会，或添加至会议列表：
    https://meeting.tencent.com/dm/QBskrCufaEy0

    #腾讯会议：173-782-419

    手机一键拨号入会
    +8675536550000,,173782419# (中国大陆)
    +85230018898,,,2,173782419# (中国香港)

    根据您的位置拨号
    +8675536550000 (中国大陆)
    +85230018898 (中国香港)
    ```

  - [12班4月14日网课视频回放地址](https://meeting.tencent.com/v2/cloud-record/share?id=86877d39-eb60-4093-9866-abd2b1fed171&from=3&is-single=true)
- 参考链接
  - [成都大学超星网络教学平台网址](http://cdu.fanya.chaoxing.com/portal)
  - https://zhibo.chaoxing.com/download
  - https://i.mooc.chaoxing.com
主题 :12班第9周信息系统分析课程会议
日期 :2022-04-14 13:10:47

## BOM样例

```text
4001产品，3001，3002半成品，2001，2002，1001原材料（需要采购）
BOM表：
4001 
  3001*2        需要6个，还需要5个
    2001*1      需要5个  还需要4个     
    2002*3      需要15个 还需要15个
      1001*2    需要30个 还需要20个
  3002*1        需要3个，还需要1个
    2001*2      需要2个  还需要2个

库存
1001  10个
2001  1个
3001  1个
3002  2个
4001  0个

生产4001 3个,需要采购多少:1001*20个，2002*15个，2001*6个
采购周期，生产需要时间
客户订单需要尽快生产，送货，何时送货？
预备货：采购原材料（多采购一些），生产多少半成品？
```
